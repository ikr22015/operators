<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
                $a = 3 * 3 % 5;
                echo $a."</br>";// (3 * 3) % 5 = 4
                // ternary operator associativity differs from C/C++
                $a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2
                echo$a."</br>";

                $a = 1;
                $b = 2;
                
                $a = $b += 3; // $a = ($b += 3) -> $a = 5, $b = 5
                echo $a."</br>";

        ?>
    </body>
</html>
